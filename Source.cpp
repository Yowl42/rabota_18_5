#include <iostream>
#include <string>

class Player
{
private:
	std::string Name;
	int Score;
public:
	Player() : Name("-"), Score(0) {}
	Player(std::string InitName, int InitScore) : Name(InitName), Score(InitScore) {}

	std::string GetName() const { return Name; }
	int GetScore() const { return Score; }

	void SetName(std::string NewName) { Name = NewName; }
	void SetScore(int NewScore) { Score = NewScore; }
};

void InsertionSort(Player** Array, int ArraySize) {
	for (int i = 1; i < ArraySize; i++) {
		int j = i;
		Player temp;
		while (j > 0 && Array[j - 1]->GetScore() < Array[j]->GetScore()) {
			temp = *Array[j-1];
			*Array[j - 1] = *Array[j];
			*Array[j] = temp;
			j--;
		}
	}
}

int main()
{
	setlocale(LC_ALL, "ru");
	int Amount = 0;
	std::cout << "���������� �������: ";
	std::cin >> Amount;
	
	Player** PlayersList = new Player* [Amount];
	for (int i = 0;i < Amount;i++)
	{		
		std::string Name;// = "p"+std::to_string(i);
		int Score;// = rand();
	
		std::cout << std::endl << "��� " << i+1 << " ������: ";
		std::cin.ignore();
		std::getline(std::cin, Name);
		std::cout << std::endl << "���� " << i+1 << " ������: ";
		std::cin >> Score;
		PlayersList[i] = new Player{ Name, Score };
	};

	InsertionSort(PlayersList, Amount);
	std::cout << "\n\n===============================\n\n";

	for (int i = 0;i < Amount;i++)
	{
		std::cout << PlayersList[i]->GetName() << "\t:\t" << PlayersList[i]->GetScore() << std::endl;
	}

	delete[] PlayersList;
}